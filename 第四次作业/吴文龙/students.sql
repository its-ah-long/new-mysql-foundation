/*
 Navicat MySQL Data Transfer

 Source Server         : zy
 Source Server Type    : MySQL
 Source Server Version : 50737
 Source Host           : localhost:3306
 Source Schema         : zy

 Target Server Type    : MySQL
 Target Server Version : 50737
 File Encoding         : 65001

 Date: 07/03/2022 19:31:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for students
-- ----------------------------
DROP TABLE IF EXISTS `students`;
CREATE TABLE `students`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `province` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `age` int(11) NULL DEFAULT NULL,
  `The product name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `The product price` decimal(10, 2) NULL DEFAULT NULL,
  `Product quantity` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `The order price` decimal(10, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of students
-- ----------------------------
INSERT INTO `students` VALUES (1, '郭靖', '广东', 33, 'y7000p', 6599.00, '3', 19797.00);
INSERT INTO `students` VALUES (2, '吴靖', '福建', 36, 'r7000p', 6899.00, '5', 34495.00);
INSERT INTO `students` VALUES (3, '郭靖明', '广东', 40, 'r9000p', 8999.00, '4', 35996.00);
INSERT INTO `students` VALUES (4, '李靖', '福建', 38, 'z7000p', 6999.00, '6', 41994.00);
INSERT INTO `students` VALUES (5, '李靖明', '福建', 26, 'r9000p', 9000.00, '2', 18000.00);
INSERT INTO `students` VALUES (6, '郭靖', '福建', 35, 'y9000p', 8888.00, '4', 26664.00);
INSERT INTO `students` VALUES (7, '郭明靖', '福建', 30, '3060', 4000.00, '10', 40000.00);

SET FOREIGN_KEY_CHECKS = 1;
