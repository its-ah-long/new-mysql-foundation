CREATE DATABASE `testdb` DEFAULT CHARACTER set utf8;

#新建表
drop table if exists `user`;
create table `user`(
user_id int PRIMARY key auto_increment COMMIT '主键',
login_name varchar(50) not null comment '登入名',
age tinyint comment '年龄',
myself text comment '自我介绍',
money decimal (10,2) not null comment '余额',
create_time datetime not null comment '注册时间',
update_time int not null comment '更新时间'
);
use testdb