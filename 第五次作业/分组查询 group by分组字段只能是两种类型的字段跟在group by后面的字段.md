# 分组查询 group by分组字段只能是两种类型的字段跟在group by后面的字段

#### 统计数据 (使用聚合函数查询的字段)类似查询省份的用户个数count查询用户 个数select +列名，count（列名） from +表名+group by +列名（列名就填你想查的东西）如果需要排序则group by +列名 +desc（或者asc）；

​       通过分两组来查询类似select +列名，count（列名），（列名） from +表名+group by +列名，列名（列名就填你想查的东西）直接放两个列名default默认值设计表那里弄设置内容为唯一的，也就是内容不能相同在设计表里设置索引里弄索引类型选定unique 索引方法选btree，名字填你想弄唯一的类型，把列名填进去UNIQUE（唯一）

外键始终选严格的（RESTRICT）并不是所有的引擎都可以设外键引擎也可以是MyISAM（快）比较快，只不过没有innoDB那么严谨如果有订单时是无法更改用户的id的！因为RESTRICT是严格的！

主表和副表类似订单表和用户表1，怎么了解谁是主表？先有谁后有谁，少了谁就不可以，谁就是主表类似没有了用户就没有订单，用户就是主导，用户就是主表。不能用主键（主表的主键）连别的表（主表）不能连不是主键（副表的主键）创建数据库CREATE DATABASE +(库的名字) +DEFAULT CHARACTER set UTF8(设置默认字符集)+COLLATE utf8_ latvian_ci(设置后面的七七八八，也可以该不一样的)(...)代表自己填入的数据语法CREATE DATABASE (...) DEFAULT CHARACTER set (...) COLLATE(...)删除数据库drop DATABASE+(库的名字)判断是否存在drop DATABASE if EXUSTS +(库的名字)if（代表如果存在那就删除这个库）新建表CREATE TABLE+'表名'(列名+类型（比如int） PRIMARY KEY(主键，有主键就不用设置 not null) auto_incremen(自动递增) COMMET '注释'列名+类型（如果用设置长度那就在类型后面跟括号，括号里面放长度多少多少，长度是数字） not null COMMET '注释'      COMMET (注释，用单引号括起来)列名+类型（10,2）（浮点型后面跟长度加小数点后几位） not null comment ‘注释’（设置默认值）列名+类型 DEFAULT NOW() NOT NULL comment ‘注释’（设置唯一值）列名+类型 UNIQUE NOT NULL comment ‘注释’)use testdb(使用表)