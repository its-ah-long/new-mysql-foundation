/*
Navicat MySQL Data Transfer

Source Server         : nnn
Source Server Version : 50730
Source Host           : 127.0.0.1:3306
Source Database       : zy

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2022-03-28 19:29:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for yhb
-- ----------------------------
DROP TABLE IF EXISTS `yhb`;
CREATE TABLE `yhb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `gmail` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yhb
-- ----------------------------
INSERT INTO `yhb` VALUES ('4', '刘大傻', '33', '123453678', '2717752667@qq.com');
INSERT INTO `yhb` VALUES ('5', '嗨害', '29', '123445678', '2717752666@qq.com');
INSERT INTO `yhb` VALUES ('10', '张三丰', '19', '123457678', '2717752661@qq.com');
INSERT INTO `yhb` VALUES ('11', '赵无极', '22', '123457678', '2717752660@qq.com');
INSERT INTO `yhb` VALUES ('12', '李乾坤', '28', '123415648', '2717752669@qq.com');
INSERT INTO `yhb` VALUES ('13', '孙行者', '27', '123425678', '2717752668@qq.com');
INSERT INTO `yhb` VALUES ('14', '行者孙', '33', '123453678', '2717752667@qq.com');
INSERT INTO `yhb` VALUES ('15', '老八', '29', '123445678', '2717752666@qq.com');
INSERT INTO `yhb` VALUES ('16', '老六', '22', '123454678', '2717752665@qq.com');
INSERT INTO `yhb` VALUES ('17', '隐族人', '26', '123415678', '2717752664@qq.com');
INSERT INTO `yhb` VALUES ('18', '宋麻花', '32', '123668952', '5265252663@qq.com');
INSERT INTO `yhb` VALUES ('19', '王八', '23', '123456778', '2717752662@qq.com');
INSERT INTO `yhb` VALUES ('20', '王九', '19', '123457678', '2717752661@qq.com');
INSERT INTO `yhb` VALUES ('21', '王十', '22', '123457678', '2717752660@qq.com');
INSERT INTO `yhb` VALUES ('22', '王一', '28', '123415648', '2717752669@qq.com');
INSERT INTO `yhb` VALUES ('23', '王二', '27', '123425678', '2717752668@qq.com');
INSERT INTO `yhb` VALUES ('24', '王三', '33', '123453678', '2717752667@qq.com');
INSERT INTO `yhb` VALUES ('25', '王四', '29', '123445678', '2717752666@qq.com');
INSERT INTO `yhb` VALUES ('26', '王五', '22', '123454678', '2717752665@qq.com');
INSERT INTO `yhb` VALUES ('27', '王六', '26', '123415678', '2717752664@qq.com');
INSERT INTO `yhb` VALUES ('28', '王七', '32', '123668952', '5265252663@qq.com');
INSERT INTO `yhb` VALUES ('29', '王八', '23', '123456778', '2717752662@qq.com');
INSERT INTO `yhb` VALUES ('30', '王九', '19', '123457678', '2717752661@qq.com');
INSERT INTO `yhb` VALUES ('31', '王十', '22', '123457678', '2717752660@qq.com');
