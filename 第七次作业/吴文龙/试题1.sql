﻿
-- 1. 查询名字中含有「风」字的学生信息
# like
SELECT * from student where sname like '%风%';

-- 2.查询「李」姓老师的数量
SELECT count(*)数量 from teacher where tname like '李%';


-- insert into teacher(tid,tname) values('04','小李');


-- 3.查询每门课程被选修的学生数
SELECT cid,count(*)学生数 from sc
GROUP BY cid

;


-- 4. 查询男生、女生人数
select ssex,count(*)人数 from student
GROUP BY ssex
;


-- 5.求每门课程的学生人数
SELECT cid,COUNT(*)人数 from sc
GROUP BY cid


-- 6. 统计每门课程的学生选修人数（超过 5 人的课程才统计）
SELECT cid,count(*)学生数 from sc
GROUP BY cid
HAVING 学生数>5
;


-- 7.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列
SELECT cid,AVG(score)平均成绩 from sc
GROUP BY cid
order by 平均成绩 desc,cid asc
;

-- 8.查询课程编号为 01 且课程成绩在 80 分以上的学生的学号和姓名
SELECT * from sc
inner join student on student.sid=sc.sid
where cid='01' and score>=80
;



-- 9.查询课程名称为「数学」，且分数低于 60 的学生姓名和分数

# 课程表 成绩表 学生表

SELECT student.sname,sc.score from course
inner join sc on sc.cid=course.cid
inner join student on student.sid=sc.sid
where cname='数学' and sc.score<60


;


-- 10.查询任何一门课程成绩在 70 分以上学生的姓名、课程名称和分数

select s.sname,c.cname,sc.score from sc
inner join student s on s.sid=sc.sid
inner join course c  on c.cid=sc.cid
where score>70


;


-- 11.查询存在不及格的课程名称
SELECT DISTINCT cname from sc
inner join course c on c.cid=sc.cid
where score<60
;


-- 12.查询至少有一门课与学号为" 01 "的同学所学 相同的同学的信息(学号，姓名)
select DISTINCT s.* from sc sc1
inner join sc sc2 on sc1.sid='01' and sc2.sid <>'01' and sc1.cid=sc2.cid
inner join student s on s.sid = sc2.sid
;




-- 13.成绩不重复，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩

SELECT student.*,sc.score from teacher
	INNER JOIN course  on  course.tid=teacher.tid
	INNER JOIN sc on   course.cid = sc.cid
	INNER JOIN student on sc.sid=student.sid
	where sc.cid='02'
	ORDER BY sc.score DESC
	LIMIT 1




-- 14.检索" 01 "课程分数小于 60，按分数降序排列的学生信息
SELECT * from sc
 INNER JOIN student s on s.sid=sc.sid
 where sc.cid='01' and sc.score<60


-- 15.查询学过「张三」老师授课的同学的信息
SELECT student.* from teacher
 INNER JOIN course c on teacher.tid=c.tid
 INNER JOIN sc on sc.cid=c.cid
 INNER JOIN student on student.sid=sc.sid
 where sc.cid='02'






-- 16.查询在 SC 表存在成绩的学生信息
SELECT * from sc
 INNER JOIN student on sc.sid=student.sid




-- 17.查询所有同学的学生编号、学生姓名、选课总数、所有课程的成绩总和

SELECT DISTINCT sc.sid,count(*)选课总数,SUM(sc.score),s.sname from sc
 INNER JOIN student s on sc.sid=s.sid
 GROUP BY sid



-- 18，并统计同名人数
SELECT COUNT(*)同名人数  from student 
 GROUP BY sname
 HAVING 同名人数>1


-- 19.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩
SELECT DISTINCT sc.sid,avg(sc.score)平均成绩,s.sname  from sc
 INNER JOIN student s on s.sid=sc.sid
 GROUP BY sid
 HAVING 平均成绩>=60

-- 20.查询没有学全所有课程的同学的信息
SELECT s.*,COUNT(*)课程数量 from sc
INNER JOIN student s on sc.sid=s.sid
GROUP BY sid
HAVING 课程数量<3







-- 21.查询平均成绩大于等于 85 的所有学生的学号、姓名和平均成绩
SELECT sc.sid,s.sname,avg(sc.score)平均成绩 from sc
INNER JOIN student s on sc.sid= s.sid
GROUP BY sid
HAVING 平均成绩>=85



-- 22.查询出只选修两门课程的学生学号和姓名
SELECT sc.sid,s.sname,COUNT(*)课程数量 from sc
INNER JOIN student s on sc.sid=s.sid
GROUP BY sid
HAVING 课程数量=2



-- 23.检索至少选修两门课程的学生学号
SELECT sc.sid,COUNT(*)课程数量 from sc
INNER JOIN student s on sc.sid=s.sid
GROUP BY sid
HAVING 课程数量>=2


-- 24.查询选修了全部课程的学生信息
SELECT s.*,COUNT(*)课程数量 from sc
INNER JOIN student s on sc.sid=s.sid
GROUP BY sid
HAVING 课程数量=3


